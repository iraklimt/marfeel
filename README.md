## PROJECT CONSIDERATIONS

Arquitecture
------------
As indicated in the frontentTest.pdf, I have avoided using third party libraries or frameworks, including css styling.
All the code is written in modern JS ECMA16 and sass (except a pure css loading animation) that is written in css.

- JS: For AJAX calls I decided to use the inbuilt XMLHttpRequest object instead of "fetch()", for avoid any browser incompatibilities.

- CSS: I wrote my styles using SASS for increase readability and a clean code. 
The styles are injected directly in the DOM by Webpack when the page is loaded.

- Webpack: I decided to use Webpack for compile and build the project, as well as for ECMA15 compatibility. 

Observations
------------
I noticed that for fetch the real fork number for a given repository, is necessary to make ajax calls (for each repo) to retrieve the data (https://api.github.com/repos/username/reponame), but for avoid complexity and rate limitations (the api allows only 60 requests/hour) I will asume that the fork number given by fork_count value from the general repos object (https://api.github.com/users/username/repos) is ok.
