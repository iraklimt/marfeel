/* inject styles in DOM via js */
import "../styles/main.scss";
import { UpdateDom } from "./update_dom";


let ajaxCall = (endpoint) => {
    return new Promise( resolve => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', endpoint);
        xhr.setRequestHeader("Accept", "application/vnd.github.v3+json");
        xhr.responseType = 'json';
        xhr.send();

        xhr.onload = () => { resolve( xhr ) };
    });
}


async function getUser() {
    let updateDom = new UpdateDom(),
        input = document.querySelector('#input'),
        username = input.value.toLowerCase().replace(/\s/g, ''),
        endpoint = `https://api.github.com/users/${username}`,
        res = await ajaxCall(endpoint);

    if( res.status !== 200 ){
        updateDom.displayError(res.response.message);
    } else {
        let repos = await getRepos(username);
        updateDom.displayUser(res.response, repos.response);
    }
}


async function getRepos(username) {
    let endpoint = `https://api.github.com/users/${username}/repos`;
    return await ajaxCall(endpoint);
}

/* Add triggers to search button and Enter keypress for perform an AJAX call*/
document.querySelector('#submit').addEventListener('click', getUser);
document.querySelector('.input-block').addEventListener('keyup', _evt => _evt.keyCode === 13 ? getUser() : null);