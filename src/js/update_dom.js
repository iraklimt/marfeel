export class UpdateDom {
    
    constructor()
    {
        this.container = document.querySelector('.result-block');
        this.searchBtn = document.querySelector('#submit');
        this.loading = document.createElement('div');
        this.loading.classList.add('loader');
        this.startLoading();
    }
    
    startLoading()
    {
        this.container.innerHTML = ''
        this.searchBtn.innerHTML = '';
        this.searchBtn.appendChild(this.loading);
    }
         
    stopLoading()
    {
        this.searchBtn.innerHTML = 'Search';
    }


    displayError(errorMessage)
    {

        let errorBlock = document.createElement('div');
        errorBlock.classList.add('error-msg');
        errorBlock.innerText = errorMessage;
        this.container.appendChild(errorBlock);

        this.stopLoading();
    }


    displayUser(user, repos)
    {
        let successBlock = document.createElement('div');
        successBlock.classList.add('success-block');

        /* USER INFO */

        let userBlock = document.createElement('div');
        userBlock.classList.add('user-block');
        successBlock.appendChild(userBlock);
        
        let img = document.createElement('div');
        img.classList.add('image');
        /* To simple value validation/existence we can use
        the OR operator (value || '') if no value, output empty space */
        img.style.backgroundImage = `url(${user.avatar_url || ''})`;
        userBlock.appendChild(img);
        
        let data = document.createElement('div');
        data.classList.add('data');
        userBlock.appendChild(data);
        
        let username = document.createElement('p');
        username.classList.add('username');
        username.appendChild(document.createTextNode(user.login || ''));
        data.appendChild(username);
        
        let fullname = document.createElement('p');
        fullname.classList.add('fullname');
        fullname.appendChild(document.createTextNode(user.name || ''));
        data.appendChild(fullname);
        
        let bio = document.createElement('p');
        bio.classList.add('bio');
        bio.appendChild(document.createTextNode(user.bio || ''));
        data.appendChild(bio);

        /* REPOS */

        let reposBlock = document.createElement('div');
        reposBlock.classList.add('repos-block');
        successBlock.appendChild(reposBlock);   
        
        let title = document.createElement('p');
        title.classList.add('title');
        title.appendChild(document.createTextNode('Repositories'));  
        reposBlock.appendChild(title); 

        if(!repos || repos.length === 0){
            let noRepos = document.createElement('p');
            noRepos.appendChild(document.createTextNode('This user has no repos'));  
            reposBlock.appendChild(noRepos);

        } else {

            repos.forEach( repo => {
                let repository = document.createElement('div');
                repository.classList.add('repo');
                reposBlock.appendChild(repository); 
        
                let name = document.createElement('div');
                name.classList.add('name');
                name.appendChild(document.createTextNode(repo.name || ''));  
                repository.appendChild(name); 
                
                let repoInfo = document.createElement('div');
                repoInfo.classList.add('info');
                /* For simplyfy SVG injection we will inject it as innerHTML */
                repoInfo.innerHTML = `<svg height="16" class="octicon octicon-star v-align-text-bottom" vertical_align="text_bottom" viewBox="0 0 14 16" version="1.1" width="14" aria-hidden="true"><path fill-rule="evenodd" d="M14 6l-4.9-.64L7 1 4.9 5.36 0 6l3.6 3.26L2.67 14 7 11.67 11.33 14l-.93-4.74L14 6z"></path></svg> ${repo.stargazers_count || ''} <svg class="octicon octicon-repo-forked v-align-text-bottom" viewBox="0 0 10 16" version="1.1" width="10" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M8 1a1.993 1.993 0 00-1 3.72V6L5 8 3 6V4.72A1.993 1.993 0 002 1a1.993 1.993 0 00-1 3.72V6.5l3 3v1.78A1.993 1.993 0 005 15a1.993 1.993 0 001-3.72V9.5l3-3V4.72A1.993 1.993 0 008 1zM2 4.2C1.34 4.2.8 3.65.8 3c0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3 10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2zm3-10c-.66 0-1.2-.55-1.2-1.2 0-.65.55-1.2 1.2-1.2.65 0 1.2.55 1.2 1.2 0 .65-.55 1.2-1.2 1.2z"></path></svg> ${repo.forks_count || ''}`;

                repository.appendChild(repoInfo);
            });

        }

        this.container.appendChild(successBlock);

        this.stopLoading();
    }
}